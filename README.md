# Promize

Deployment of keep services

### Services

Six main servies now

```
1. elassandra
2. redis
3. kibana
4. aplos
5. lokka
6. gateway
7. notifier
```

### Configure .env

Change `host.docker.local` field in `.env` file to local machines ip. Also 
its possible to add a host entry to `/etc/hosts` file by overriding
`host.docker.local` with local machines ip.

```
#/etc/hosts file
10.4.1.104    host.docker.local
```

### Deploy services

Start services in following order

```
docker-compose up -d elassandra
docker-compose up -d redis
docker-compose up -d kibana // optional to deploy
docker-compose up -d aplos
docker-compose up -d lokka // optional to deploy
docker-compose up -d gateway
docker-compose up -d notifier
```

### Connect apps

Gateway service will start a REST api on `7654` port. For an example if your
machines ip is `10.4.1.15` the apis can be access via `10.4.1.15:7654/api/<apiname>`.
Add this config to mobile/web app and connect to the services.
